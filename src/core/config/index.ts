export default {
  DATABASE: process.env.DATABASE ?? '',
  ENDPOINT_PREFIX: process.env.ENDPOINT_PREFIX ?? '',
  PORT: process.env.PORT ?? 7000,
  MODE: process.env.MODE,
};
