import { Request, Response } from 'express';
import * as service from './note.service';

export const store = async (req: Request, res: Response) => {
  const response = await service.store(req.body);
  res.json(response);
};

export const read = async (req: Request, res: Response) => {
  const response = await service.read();
  res.json(response);
};