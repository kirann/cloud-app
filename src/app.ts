import express, { json, urlencoded } from 'express';
import 'express-async-errors';
import config from './core/config';
import cors from 'cors';
import { errorHandler } from './core/handlers/errorHandler';
import routes from './core/loaders/routes';
import fileUpload from 'express-fileupload';

const app = express();
app.use(json());
app.use(urlencoded({ extended: true }));
//Static Folder enable
// app.use(express.static('./public'));

//file upload
app.use(fileUpload());

//Allow CORS
app.use(cors());

app.use(config.ENDPOINT_PREFIX, routes());

app.use(errorHandler);

export default app;
