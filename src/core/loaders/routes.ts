import { Router } from 'express';
import noteRoute from '../../features/note/note.routes';

const app = Router();

// Register all routes here
export default () => {
  app.use('/note', noteRoute);
  return app;
};
