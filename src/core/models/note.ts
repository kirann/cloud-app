import mongoose, { Schema, Document } from 'mongoose';

export interface INote extends Document {
  title: string;
}

const schema = new Schema(
  {
    title: {
      type: String,
      required: 'Title is required',
      index: true,
    },
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  }
);

export default mongoose.model<INote>('Note', schema, 'notes');
