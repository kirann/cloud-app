"use strict";
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    DATABASE: (_a = process.env.DATABASE) !== null && _a !== void 0 ? _a : '',
    ENDPOINT_PREFIX: (_b = process.env.ENDPOINT_PREFIX) !== null && _b !== void 0 ? _b : '',
    PORT: (_c = process.env.PORT) !== null && _c !== void 0 ? _c : 7000,
    MODE: process.env.MODE,
};
