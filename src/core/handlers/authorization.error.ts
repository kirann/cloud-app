import { CustomError } from './custom.error';

export class AuthorizationError extends CustomError {
  statusCode = 401;

  serializeErrors() {
    return { message: 'You are not authorized for this action 🚫🚫' };
  }

  constructor() {
    super('Authorization failure!');
  }
}
