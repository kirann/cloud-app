import mongoose from 'mongoose';
import { INote } from '../../core/models/note';

const Note = mongoose.model<INote>('Note');

export const store = async (payload: any) => {
  const newStory = new Note({
    ...payload,
  });

  await newStory.save();

  return {
    message: 'Note created successfully',
    identifier: newStory?._id,
    title: newStory?.title,
  };
};

export const read = async () => {
  const findMine = await Note.find(
    {},
    'title created_at updated_at'
  )
    .lean();

  if (!findMine || !findMine.length) throw new Error('No notes found');
  return findMine;
};
