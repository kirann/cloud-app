import { Router } from 'express';
import * as controller from './note.controller';
import { body } from 'express-validator';
import checkValidation from '../../core/handlers/checkValidation';
const router = Router();

router.post(
  '/',
  body('title', 'Title is required').notEmpty(),
  checkValidation,
  controller.store
);

router.get('/', controller.read);

export default router;
