"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.read = exports.store = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const Note = mongoose_1.default.model('Note');
const store = (payload) => __awaiter(void 0, void 0, void 0, function* () {
    const newStory = new Note(Object.assign({}, payload));
    yield newStory.save();
    return {
        message: 'Note created successfully',
        identifier: newStory === null || newStory === void 0 ? void 0 : newStory._id,
        title: newStory === null || newStory === void 0 ? void 0 : newStory.title,
    };
});
exports.store = store;
const read = () => __awaiter(void 0, void 0, void 0, function* () {
    const findMine = yield Note.find({}, 'title created_at updated_at')
        .lean();
    if (!findMine || !findMine.length)
        throw new Error('No notes found');
    return findMine;
});
exports.read = read;
